package com.cjq.utils;

import com.cjq.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@Component
public class HtmlParseUtils {
  /*  public static void main(String[] args) throws Exception {
        new HtmlParseUtils().parseJD("vue").forEach(System.out::println);
    }*/

    public List<Content> parseJD(String keywords) throws Exception {
        String url="https://search.jd.com/Search?keyword="+keywords;
        Document document = Jsoup.parse(new URL(url), 3000);
        Element element = document.getElementById("J_goodsList");
//        System.out.println(element.html());

        Elements elements = element.getElementsByTag("li");


        ArrayList<Content> goodsList= new ArrayList<>();
        //el 每一个li标签
        for (Element el : elements) {
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price =el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();
/*
            System.out.println(img);
            System.out.println(price);
            System.out.println(title);
            System.out.println("================================================================");*/
            Content content = new Content();
            content.setImg(img);
            content.setTitle(title);
            content.setPrice(price);
            goodsList.add(content);
        }
        return goodsList;

    }
}
