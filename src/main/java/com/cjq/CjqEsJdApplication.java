package com.cjq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CjqEsJdApplication {

    public static void main(String[] args) {
        SpringApplication.run(CjqEsJdApplication.class, args);
    }

}
